let mongoose          = require('mongoose'),
    Schema            = mongoose.Schema;

let LoginReportSchema = new Schema({
    account_type: { type: String },
    user_id: { type: String },
    user_code: { type: String },
    username: { type: String },
    access_level: { type: Number },
    number_of_logins: { type: Number },
    formattedLastLoginDate: { type: String },
    last_login: { type: String }
});

// return the model
module.exports = mongoose.model('LoginReport', LoginReportSchema);
