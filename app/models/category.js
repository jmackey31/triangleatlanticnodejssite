let mongoose          = require('mongoose'),
    Schema            = mongoose.Schema;

let CategorySchema = new Schema({
    category_code: { type: String },
    category_description: { type: String },
    category_image: { type: String },
    formattedDate: { type: String },
    created: { type: Date },
    updated: [Date]
});

// return the model
module.exports = mongoose.model('Category', CategorySchema);
