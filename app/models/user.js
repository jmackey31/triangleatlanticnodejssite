let mongoose          = require('mongoose'),
    Schema            = mongoose.Schema;

let UserSchema = new Schema({
    old_id: { type: String },
    account_type: { type: String },
    fac_name: { type: String },
    contact_person: { type: String },
    user_code: { type: String },
    access_level: { type: Number },
    username: { type: String, required: true, index: { unique: true }},
    // Passwords will not be hashed by request from the client. LOL.  I'm not kidding either.
    password: { type: String, required: true, select: false },
    parent_account: {
        user_code: { type: String },
        contact_person: { type: String }
    },
    old_parent_account_string: { type: String },
    email: { type: String },
    phone: { type: String },
    fax: { type: Number },
    firstname: { type: String },
    lastname: { type: String },
    address_1: { type: String },
    address_2: { type: String },
    city: { type: String },
    state: { type: String },
    zip: { type: Number },
    zip_cod_string: { type: String },
    web: { type: String },
    price_rounding: { type: Number, default: 0 },
    opt_out_base_price_increases: { type: Boolean, default: false, select: false },
    account_status:  { type: Number, required: true, default: 777 },
    formattedDate: { type: String },
    created: { type: Date },
    updated: [Date]
});

// return the model
module.exports = mongoose.model('User', UserSchema);
