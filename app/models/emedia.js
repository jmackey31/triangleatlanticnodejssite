let mongoose          = require('mongoose'),
    Schema            = mongoose.Schema;

let EMediaSchema = new Schema({
    image_name: { type: String },
    image_caption: { type: String },
    file_name: { type: String },
    formattedDate: { type: String },
    created: { type: Date },
    updated: [Date]
});

// return the model
module.exports = mongoose.model('EMedia', EMediaSchema);
