let todays = new Date();

// Server Validations

// function to check values before inserting in query
module.exports.validateAnyValue = function(valueToCheck) {
    if (
        (valueToCheck !== null) &&
        (valueToCheck !== 'null') &&
        (valueToCheck !== 'NaN') &&
        (valueToCheck !== '') &&
        (valueToCheck !== 'none') &&
        (valueToCheck !== undefined) &&
        (valueToCheck !== 'undefined') &&
        (valueToCheck !== false) &&
        (valueToCheck !== 'false') &&
        (valueToCheck !== 'Select State') &&
        (valueToCheck !== 'Select City')
      ) {
        // console.log('valueToCheck returned TRUE see it here:');
        // console.log(valueToCheck);
        return true;
    } else {
        // console.log('valueToCheck returned FALSE see it here:');
        // console.log(valueToCheck);
        return false;
    }
}
