let express           = require('express'),
    app               = express(),
    bodyParser        = require('body-parser'),
    morgan            = require('morgan'),
    cookieParser      = require('cookie-parser'),
    session           = require('express-session'),
    mongoose          = require('mongoose'),
    mongoosePaginate  = require('mongoose-paginate'),
    dotenv            = require('dotenv').config(),
    config            = require('./config'),
    path              = require('path'),
    passport          = require('passport'),
    LocalStrategy     = require('passport-local').Strategy,
    methodOverride    = require('method-override');

app.use(methodOverride());

// APP CONFIGURATION
// use body parser so we can grap information from POST requests
app.use(bodyParser.json({limit: '50mb'}));
// app.use(bodyParser.urlencoded({limit: '50mb', extended: true}));
// Also need to set the nginx configuration file settings.
// http://www.codingslover.com/2015/05/413-request-entity-too-large-error-with.html
// vim /etc/nginx/nginx.conf
// # set client body size to 20M #
// client_max_body_size 20M;
// service nginx restart
app.use(bodyParser.urlencoded({limit: '50mb', extended: true, parameterLimit: 50000}));
app.use(bodyParser.json());

// Set the middleware to change the name 'X-Powered-By', 'Express' to something a little more pleasing to the eye
app.use(function (req, res, next) {
  res.set('X-Powered-By', 'Triangle Atlantic');
  next();
});

app.use(cookieParser(process.env.COOKIE_PARSER_STRING));
app.use(session({ secret: process.env.SESSION_SECRET }));

app.use(passport.initialize());
app.use(passport.session());

// configure our app to handle CORS requests
app.use(function(req, res, next) {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST');
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With, content-type, \ Authorization');
    next();
});

// log all requests to the console
app.use(morgan('dev'));

// connect to the database
mongoose.connect(config.database);

// set static files location
// used for requests that our frontend will make
app.use(express.static(__dirname + '/public'));

// API ROUTES -------------------------
let apiRoutes = require('./app/routes/api')(app, express);
app.use('/api', apiRoutes);

// MAIN CATCHALL ROUTE ----------------
// SEND USERS TO THE FRONTEND ---------
// has to be registered after API ROUTES
app.get('*', function(req, res) {
    res.sendFile(path.join(__dirname + '/public/app/views/index.html'));
});

// START THE SERVER
app.listen(config.port);
console.log('View the project on: http://localhost:' + config.port);
