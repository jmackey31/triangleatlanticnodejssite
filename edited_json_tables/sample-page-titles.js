var pageTitlesArr = [
    'About TACC',
    'Our Catalog',
    'Login',
    'Mailing List',
];

var adminPageTitlesArr = [
    'Main Menu',
    'Add Item',
    'Add Category',
    'Create Sales Rep',
    'Create Funeral Home',
    'Edit Master Information',
    'Remove A Category',
    'Manage User Accounts',
    'Login Reports',
    'Upload Media Library',
    'View User Credentials',
    'Price Management',
    'Public Account Setup',
    'Change Administrator Password'
];
