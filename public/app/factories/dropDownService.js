angular.module('dropDownService', [])
    .factory('DropDown', function() {

    return {

        // ____________________________________________________
        // Time Picker
        timeRange: [
            '12:00 AM',
            '12:30 AM',
            '01:00 AM',
            '01:30 AM',
            '02:00 AM',
            '02:30 AM',
            '03:00 AM',
            '03:30 AM',
            '04:00 AM',
            '04:30 AM',
            '05:00 AM',
            '05:30 AM',
            '06:00 AM',
            '06:30 AM',
            '07:00 AM',
            '07:30 AM',
            '08:00 AM',
            '08:30 AM',
            '09:00 AM',
            '09:30 AM',
            '10:00 AM',
            '10:30 AM',
            '11:00 AM',
            '11:30 AM',
            '12:00 PM',
            '12:30 PM',
            '01:00 PM',
            '01:30 PM',
            '02:00 PM',
            '02:30 PM',
            '03:00 PM',
            '03:30 PM',
            '04:00 PM',
            '04:30 PM',
            '05:00 PM',
            '05:30 PM',
            '06:00 PM',
            '06:30 PM',
            '07:00 PM',
            '07:30 PM',
            '08:00 PM',
            '08:30 PM',
            '09:00 PM',
            '09:30 PM',
            '10:00 PM',
            '10:30 PM',
            '11:00 PM',
            '11:30 PM'
        ],

        // ____________________________________________________

        // States Object
        statesObject: [
            {
                name: "Alabama",
                abbreviation: "AL"
            },
            {
                name: "Alaska",
                abbreviation: "AK"
            },
            {
                name: "American Samoa",
                abbreviation: "AS"
            },
            {
                name: "Arizona",
                abbreviation: "AZ"
            },
            {
                name: "Arkansas",
                abbreviation: "AR"
            },
            {
                name: "California",
                abbreviation: "CA"
            },
            {
                name: "Colorado",
                abbreviation: "CO"
            },
            {
                name: "Connecticut",
                abbreviation: "CT"
            },
            {
                name: "Delaware",
                abbreviation: "DE"
            },
            {
                name: "District Of Columbia",
                abbreviation: "DC"
            },
            {
                name: "Federated States Of Micronesia",
                abbreviation: "FM"
            },
            {
                name: "Florida",
                abbreviation: "FL"
            },
            {
                name: "Georgia",
                abbreviation: "GA"
            },
            {
                name: "Guam",
                abbreviation: "GU"
            },
            {
                name: "Hawaii",
                abbreviation: "HI"
            },
            {
                name: "Idaho",
                abbreviation: "ID"
            },
            {
                name: "Illinois",
                abbreviation: "IL"
            },
            {
                name: "Indiana",
                abbreviation: "IN"
            },
            {
                name: "Iowa",
                abbreviation: "IA"
            },
            {
                name: "Kansas",
                abbreviation: "KS"
            },
            {
                name: "Kentucky",
                abbreviation: "KY"
            },
            {
                name: "Louisiana",
                abbreviation: "LA"
            },
            {
                name: "Maine",
                abbreviation: "ME"
            },
            {
                name: "Marshall Islands",
                abbreviation: "MH"
            },
            {
                name: "Maryland",
                abbreviation: "MD"
            },
            {
                name: "Massachusetts",
                abbreviation: "MA"
            },
            {
                name: "Michigan",
                abbreviation: "MI"
            },
            {
                name: "Minnesota",
                abbreviation: "MN"
            },
            {
                name: "Mississippi",
                abbreviation: "MS"
            },
            {
                name: "Missouri",
                abbreviation: "MO"
            },
            {
                name: "Montana",
                abbreviation: "MT"
            },
            {
                name: "Nebraska",
                abbreviation: "NE"
            },
            {
                name: "Nevada",
                abbreviation: "NV"
            },
            {
                name: "New Hampshire",
                abbreviation: "NH"
            },
            {
                name: "New Jersey",
                abbreviation: "NJ"
            },
            {
                name: "New Mexico",
                abbreviation: "NM"
            },
            {
                name: "New York",
                abbreviation: "NY"
            },
            {
                name: "North Carolina",
                abbreviation: "NC"
            },
            {
                name: "North Dakota",
                abbreviation: "ND"
            },
            {
                name: "Northern Mariana Islands",
                abbreviation: "MP"
            },
            {
                name: "Ohio",
                abbreviation: "OH"
            },
            {
                name: "Oklahoma",
                abbreviation: "OK"
            },
            {
                name: "Oregon",
                abbreviation: "OR"
            },
            {
                name: "Palau",
                abbreviation: "PW"
            },
            {
                name: "Pennsylvania",
                abbreviation: "PA"
            },
            {
                name: "Puerto Rico",
                abbreviation: "PR"
            },
            {
                name: "Rhode Island",
                abbreviation: "RI"
            },
            {
                name: "South Carolina",
                abbreviation: "SC"
            },
            {
                name: "South Dakota",
                abbreviation: "SD"
            },
            {
                name: "Tennessee",
                abbreviation: "TN"
            },
            {
                name: "Texas",
                abbreviation: "TX"
            },
            {
                name: "Utah",
                abbreviation: "UT"
            },
            {
                name: "Vermont",
                abbreviation: "VT"
            },
            {
                name: "Virgin Islands",
                abbreviation: "VI"
            },
            {
                name: "Virginia",
                abbreviation: "VA"
            },
            {
                name: "Washington",
                abbreviation: "WA"
            },
            {
                name: "West Virginia",
                abbreviation: "WV"
            },
            {
                name: "Wisconsin",
                abbreviation: "WI"
            },
            {
                name: "Wyoming",
                abbreviation: "WY"
            }
        ]
        // ____________________________________________________

    };

});
