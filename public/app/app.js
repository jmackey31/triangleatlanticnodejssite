angular.module('taApp', [
    'ngAnimate',
    'ngCookies',
    'ngResource',
    'ngRoute',
    'ngSanitize',
    'ngTouch',
    'ui.router',
    'app.routes',
    'mainCtrl',
    'navbarCtrl',
    'authService',
    'httpService',
    'dropDownService',
    'formFactory',
    'fileReaderDir'
])
// Application configuration to integrate token into requests
.config(function($httpProvider) {
    // Auth interceptor to the http requests
    $httpProvider.interceptors.push('AuthInterceptor');
});
