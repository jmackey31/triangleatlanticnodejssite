-- phpMyAdmin SQL Dump
-- version 4.3.8
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jul 16, 2017 at 08:47 PM
-- Server version: 5.5.51-38.2
-- PHP Version: 5.6.20

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `setech13_tacc12_eshowroom`
--

-- --------------------------------------------------------

--
-- Table structure for table `Show_room`
--

CREATE TABLE IF NOT EXISTS `Show_room` (
  `id` int(11) NOT NULL,
  `usr_id` int(11) NOT NULL,
  `room_title` varchar(60) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=126 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `Show_room`
--

INSERT INTO `Show_room` (`id`, `usr_id`, `room_title`) VALUES
(18, 46, 'Women'),
(19, 46, 'Men'),
(24, 68, 'Male'),
(25, 68, 'Female'),
(26, 70, 'Male'),
(27, 70, 'Female'),
(28, 72, 'Male'),
(29, 72, 'Female'),
(31, 104, 'Male'),
(32, 106, 'Bentley Show Room'),
(33, 106, 'Anchor Showroom '),
(34, 106, '18 Ga Showroom'),
(35, 106, 'Oversize'),
(36, 107, 'Anchor'),
(37, 108, 'FH Floor'),
(42, 108, 'Cremation'),
(44, 113, 'Male'),
(45, 113, 'Female'),
(46, 125, 'Economy Showroom'),
(47, 115, 'Latino'),
(48, 115, 'Williiford Selection Room'),
(49, 115, 'Cremation'),
(50, 115, 'Excellence Burial Package'),
(51, 115, 'Premium Burial Package'),
(52, 115, 'Standard Burial Package'),
(53, 145, 'Showroom '),
(55, 150, 'Selection Room'),
(56, 218, 'JEB picks'),
(59, 230, '20 ga sealer'),
(60, 230, '20 ga non sealer'),
(61, 230, 'Tall & Wide'),
(62, 232, 'Male'),
(63, 233, 'Hardwoods'),
(64, 238, 'Male'),
(65, 131, 'Family Selection Room'),
(66, 97, 'Showroom'),
(67, 248, 'Men'),
(68, 252, 'Corners For Spring Hope'),
(69, 272, 'wood wall'),
(70, 272, 'High metal wall'),
(71, 272, 'Low Wall '),
(72, 273, 'Wood Wall'),
(73, 273, 'Basic Metals'),
(74, 273, 'High End Metals'),
(75, 273, 'Cremation'),
(76, 273, 'Table Area'),
(77, 279, 'Special Program'),
(78, 281, 'testing'),
(79, 283, 'Oversize'),
(80, 288, 'Powles'),
(81, 288, 'Wood'),
(82, 288, 'Veterans Funeral Care'),
(84, 204, 'Heartland'),
(85, 305, ''),
(87, 282, 'Basic'),
(88, 282, 'Your choice'),
(89, 148, 'Anchor selection'),
(90, 357, 'Main'),
(94, 370, 'Blue'),
(96, 376, 'Bentley Collection'),
(97, 376, 'Anchor Collection'),
(98, 376, 'Spartan Collection'),
(99, 376, 'Bayside Collection'),
(100, 376, 'Hardwood Collection'),
(101, 376, 'Military Collection'),
(102, 376, 'Specialty Collection'),
(110, 378, 'Veteran Tribute Room'),
(111, 378, 'Main Selection Room'),
(112, 380, 'Davie Funeral Service'),
(113, 353, 'Standard '),
(114, 391, 'SouthEast'),
(115, 366, ''),
(116, 366, 'Sullivans'),
(117, 370, 'Donaldson'),
(118, 130, '1337'),
(119, 411, 'Masculine'),
(120, 411, 'Feminine'),
(121, 411, 'Proportions'),
(123, 413, '20 Ga Non-Gasketed'),
(124, 413, '20 GA Gasketed'),
(125, 408, 'SMFH Selection Room ');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `Show_room`
--
ALTER TABLE `Show_room`
  ADD PRIMARY KEY (`id`), ADD KEY `room_title` (`room_title`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `Show_room`
--
ALTER TABLE `Show_room`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=126;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
