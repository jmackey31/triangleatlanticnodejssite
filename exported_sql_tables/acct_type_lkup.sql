-- phpMyAdmin SQL Dump
-- version 4.3.8
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jul 16, 2017 at 08:52 PM
-- Server version: 5.5.51-38.2
-- PHP Version: 5.6.20

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `setech13_tacc12_eshowroom`
--

-- --------------------------------------------------------

--
-- Table structure for table `acct_type_lkup`
--

CREATE TABLE IF NOT EXISTS `acct_type_lkup` (
  `code` int(11) NOT NULL,
  `type_desc` varchar(50) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `acct_type_lkup`
--

INSERT INTO `acct_type_lkup` (`code`, `type_desc`) VALUES
(1, 'System Admin'),
(2, 'Reseller '),
(3, 'General Public'),
(4, 'Sales Rep.');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `acct_type_lkup`
--
ALTER TABLE `acct_type_lkup`
  ADD PRIMARY KEY (`code`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
