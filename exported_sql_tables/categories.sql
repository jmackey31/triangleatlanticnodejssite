-- phpMyAdmin SQL Dump
-- version 4.3.8
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jul 16, 2017 at 08:46 PM
-- Server version: 5.5.51-38.2
-- PHP Version: 5.6.20

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `setech13_tacc12_eshowroom`
--

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE IF NOT EXISTS `categories` (
  `cat_code` int(11) NOT NULL,
  `cat_desc` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `thmb_img` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=MyISAM AUTO_INCREMENT=42 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`cat_code`, `cat_desc`, `thmb_img`) VALUES
(1, 'Hardwoods', 'images/thumbs/1400.png'),
(2, 'Precious Metals', 'images/thumbs/pm-thumb.jpg'),
(3, '18 Gauge ', 'images/thumbs/1805nb.jpg'),
(4, '20 Gauge Gasketed', 'images/thumbs/2048wb.jpg'),
(5, '20 Gauge Non-Gasketed', 'images/thumbs/2206s.jpg'),
(6, 'Oversize and Tall/Wides', 'images/thumbs/2004car.jpg'),
(7, 'Cremation/Cloth ', 'images/thumbs/1203b.jpg'),
(8, 'Specialty', 'images/thumbs/specialty.jpg'),
(39, 'Panel Inserts', 'images/thumbs/mother rose pink.jpg'),
(41, '', 'images/33b463fa627cb51fe5c1ca0d322bfb03.jpg');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`cat_code`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `cat_code` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=42;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
